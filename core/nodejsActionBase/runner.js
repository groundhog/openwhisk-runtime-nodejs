/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Object which encapsulates a first-class function, the user code for an action.
 *
 * This file (runner.js) must currently live in root directory for nodeJsAction.
 */
const fs = require('fs');
const path = require('path');
const readline = require('readline');

var ffi = require('ffi-napi');
var groundhog_functions = ffi.Library('/bin/build/groundhog-tracee-lib', {
       "checkpoint_me": [ "void" , [] ],
       "restore_me": [ "void" , [] ],
       "dump_stats_me": [ "void" , [] ],
});

function snapshot(){
       //global.gc();
       groundhog_functions.checkpoint_me();
       process.stdin.resume();
       //global.gc();
}

function restore(){
       groundhog_functions.restore_me();
       // Next instructions will be the ones after the snapshot()
       // For groundhogNOP
       process.stdin.resume();
       //global.gc();
}

function dump_stats(){
       groundhog_functions.dump_stats_me();
}

const rl = readline.createInterface({
    input: process.stdin,
    //terminal: true
});

/** Initializes the handler for the user function. */
function initializeActionHandler(message) {

    if (message.binary) {
        // The code is a base64-encoded zip file.
        return unzipInTmpDir(message.code)
            .then(moduleDir => {
                let parts = splitMainHandler(message.main);
                if (parts === undefined) {
                    // message.main is guaranteed to not be empty but be defensive anyway
                    return Promise.reject('Name of main function is not valid.');
                }

                // If there is only one property in the "main" handler, it is the function name
                // and the module name is specified either from package.json or assumed to be index.js.
                let [index, main] = parts;

                // Set the executable directory to the project dir.
                process.chdir(moduleDir);

                if (index === undefined && !fs.existsSync('package.json') && !fs.existsSync('index.js')) {
                    return Promise.reject('Zipped actions must contain either package.json or index.js at the root.');
                }

                //  The module to require.
                let whatToRequire = index !== undefined ? path.join(moduleDir, index) : moduleDir;
                let handler = eval('require("' + whatToRequire + '").' + main);
                return assertMainIsFunction(handler, message.main);
            })
            .catch(error => Promise.reject(error));
    } else try {
        let handler = eval(
            `(function(){
                             ${message.code}
                             try {
                                 return ${message.main}
                             } catch (e) {
                                 if (e.name === 'ReferenceError') {
                                        return module.exports.${message.main} || exports.${message.main}
                                 } else throw e
                             }
                     })()`);
        return assertMainIsFunction(handler, message.main);
    } catch (e) {
        return Promise.reject(e);
    }
}

class NodeActionRunner {

    constructor(handler) {
        this.userScriptMain = handler;
    }

    run(args) {
        return new Promise((resolve, reject) => {
            try {
                var result = this.userScriptMain(args);
            } catch (e) {
                reject(e);
            }

            this.finalizeResult(result, resolve);
        });
    };

    finalizeResult(result, resolve) {
        // Non-promises/undefined instantly resolve.
        Promise.resolve(result).then(resolvedResult => {
            // This happens, e.g. if you just have "return;"
            if (typeof resolvedResult === "undefined") {
                resolvedResult = {};
            }
            resolve(resolvedResult);
        }).catch(error => {
            // A rejected Promise from the user code maps into a
            // successful promise wrapping a whisk-encoded error.

            // Special case if the user just called "reject()".
            if (!error) {
                resolve({error: {}});
            } else {
                const serializeError = require('serialize-error');
                resolve({error: serializeError(error)});
            }
        });
    }
}

/**
 * Copies the base64 encoded zip file contents to a temporary location,
 * decompresses it and returns the name of that directory.
 *
 * Note that this makes heavy use of shell commands because the environment is expected
 * to provide the required executables.
 */
function unzipInTmpDir(zipFileContents) {
    const mkTempCmd = "mktemp -d XXXXXXXX";
    return exec(mkTempCmd).then(tmpDir => {
        return new Promise((resolve, reject) => {
            const zipFile = path.join(tmpDir, "action.zip");
            fs.writeFile(zipFile, zipFileContents, "base64", err => {
                if (!err) resolve(zipFile);
                else reject("There was an error reading the action archive.");
            });
        });
    }).then(zipFile => {
        return exec(mkTempCmd).then(tmpDir => {
            return exec("unzip -qq " + zipFile + " -d " + tmpDir)
                .then(res => path.resolve(tmpDir))
                .catch(error => Promise.reject("There was an error uncompressing the action archive."));
        });
    });
}

/** Helper function to run shell commands. */
function exec(cmd) {
    const child_process = require('child_process');

    return new Promise((resolve, reject) => {
        child_process.exec(cmd, (error, stdout, stderr) => {
            if (!error) {
                resolve(stdout.trim());
            } else {
                reject(stderr.trim());
            }
        });
    });
}

/**
 * Splits handler into module name and path to main.
 * If the string contains no '.', return [ undefined, the string ].
 * If the string contains one or more '.', return [ string up to first period, rest of the string after ].
 */
function splitMainHandler(handler) {
    let matches = handler.match(/^([^.]+)$|^([^.]+)\.(.+)$/);
    if (matches && matches.length == 4) {
        let index = matches[2];
        let main = matches[3] || matches[1];
        return [index, main]
    } else return undefined
}

function assertMainIsFunction(handler, name) {
    if (typeof handler === 'function') {
        return Promise.resolve(handler);
    } else {
        return Promise.reject("Action entrypoint '" + name + "' is not a function.");
    }
}

global.count_invocations = -1;
global.action_handler = undefined;
rl.on('line', function(data) {
    global.count_invocations += 1;
    //console.error(data.toString());
    if (!global.count_invocations){
        // initialization
        var message = JSON.parse(data.toString().trim());
            if (message.env && typeof message.env == 'object') {
                Object.keys(message.env).forEach(k => {
                    let val = message.env[k];
                    if (typeof val !== 'object' || val == null) {
                        process.env[k] = val ? val.toString() : "";
                    } else {
                        process.env[k] = JSON.stringify(val);
                    }
                });
            }

            try {
                //console.error(JSON.parse(data.toString().trim()));
                initializeActionHandler(message)
                    .then(handler => {
                        global.action_handler = new NodeActionRunner(handler);
                        //console.log("Success");
                        //fs.write(3, "Success\n");
                var data = "Success\r\n";
                fs.write(3, Buffer.from(data), 0, data.length, null,
                    function(err, bytesWritten, buffer) {
                       if(err) return failure();
                 });

                    })
                    .catch(error => {
                        //console.log('Error');
                        //fs.write(3, "Error\n");
                        var data = "Error\r\n";
                        fs.write(3, Buffer.from(data), 0, data.length, null,
                            function(err, bytesWritten, buffer) {
                           if(err) return failure();
                         });
                    });
            }catch (error){
                console.error(error);
            }
            //console.log("Success");
        }else{
            // invocations
            var msg = JSON.parse(data.toString().trim());
            Object.keys(msg).forEach(k => {
          if (typeof msg[k] === 'string' && k !== 'value') {
            let envVariable = '__OW_' + k.toUpperCase();
          process.env[envVariable] = msg[k];
        }
      });
            //console.error(data.toString());
            if (msg.value.hasOwnProperty('groundhog-dump-stats')){
                dump_stats();
	    }
            var startTime = process.hrtime();
            global.action_handler.run(msg.value)
            .then(result => {
                var elapsed = process.hrtime(startTime);
                var TrueTime = elapsed[0] * 1e9 + elapsed[1];
                result["TrueTime"] = TrueTime;
                var response = JSON.stringify(result)+"\r\n";
                //console.error(response);
		process.stdin.pause()
                fs.write(3, Buffer.from(response), 0, 'utf8',
                    function(err, bytesWritten, buffer) {
                      if(err)
                        {
                            return "ERROR!";
                        }
		      if (global.count_invocations == 1){
                          //setTimeout(snapshot, 0);
                          setImmediate(snapshot);
                      }
                      if (global.count_invocations > 1){
                         //setTimeout(restore, 0);
                         setImmediate(restore);
	              }

                 });


            }).catch(error => {
                        console.error(error);
                        //console.log('Error');
                        //fs.write(3, "Error\n");
                var data = "Error\n";
                fs.write(3, Buffer.from(data), 0, data.length, null,
                    function(err, bytesWritten, buffer) {
                        if(err) return failure();
                    });
            });
    }
});
